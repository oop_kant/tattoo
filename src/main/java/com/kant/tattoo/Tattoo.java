/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.tattoo;

/**
 *
 * @author MSI
 */
public class Tattoo { //สร้าง class ที่มีชื่อว่า Tattoo
    String artistname; //ประกาศตัวแปร artistname เพื่อเอาไว้เก็บค่าของชื่อของช่างสัก
    int income; //ประกาศตัวแปร income เพื่อเอาไว้กับค่าของรายได้
    int price; //ประกาศตัวแปร price เพื่อนเอาไว้เก็บค่าของราคารอยสัก
    Tattoo(String artistname ,int income){ //สร้าง method ที่มีชื่อว่า Tattoo ที่มีค่า artistname และ income อยู่ข้างใน เพื่อเอาไว้ใช้ในการสร้าง object
        this.artistname = artistname; //นำค่าที่อยู่ใน artistname ตอนสร้าง object มาไว้ใน artistname
        this.income = income; //นำค่าที่อยู่ใน income ตอนสร้าง object มาไว้ใน income
    }
    
    void details(int size , int color){ //สร้าง method details รับค่า size และ color เพื่อเอาไว้เช็คในคำสั่ง if
        if(size >= 1 && size <= 25){  //ใช้คำสั่ง if เพื่อเช็คค่า size ถ้า size มีค่ามากกว่าเท่ากับ 1 และ น้อยกว่าเท่ากับ 25 ถ้าเป็นจริงให้เช็คคำสั่ง if ที่อยู่ข้างใน
            if(color == 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่าเท่ากับ 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 500
                price = 500; //price จะมีค่าเท่ากับ 500
                
            }
            if(color > 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่ามากกว่า 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 750
                price = 750; //price จะมีค่าเท่ากับ 750
                
            }
            
            
        }
        if(size >= 26 && size <=50){ //ใช้คำสั่ง if เพื่อเช็คค่า size ถ้า size มีค่ามากกว่าเท่ากับ 26 และ น้อยกว่าเท่ากับ 50 ถ้าเป็นจริงให้เช็คคำสั่ง if ที่อยู่ข้างใน
            if(color == 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่าเท่ากับ 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 700
                price = 700; //price จะมีค่าเท่ากับ 700
                
            }
            if(color > 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่ามากกว่า 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 1050
                price = 1050; //price จะมีค่าเท่ากับ 1050
                
            }
        }
        
        if(size >= 51 && size <= 100){ //ใช้คำสั่ง if เพื่อเช็คค่า size ถ้า size มีค่ามากกว่าเท่ากับ 1 และ น้อยกว่าเท่ากับ 25 ถ้าเป็นจริงให้เช็คคำสั่ง if ที่อยู่ข้างใน
            if(color == 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่าเท่ากับ 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 1000
                price = 1000; //price จะมีค่าเท่ากับ 1000
                
            }
            if(color > 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่ามากกว่า 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 1500
                price = 1500; //price จะมีค่าเท่ากับ 1500
                
            }
        }
        if(size > 100){ //ใช้คำสั่ง if เพื่อเช็คค่า size ถ้า size มีค่ามากกว่าเท่ากับ 1 และ น้อยกว่าเท่ากับ 25 ถ้าเป็นจริงให้เช็คคำสั่ง if ที่อยู่ข้างใน
            if(color == 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่าเท่ากับ 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 2000
                price = 2000; //price จะมีค่าเท่ากับ 2000
            }
            if(color > 1){ //ใช้คำสั่ง if เพื่อเช็คค่า color ถ้า color มีค่ามากกว่า 1 ถ้าเป็นจริง ตัวแปร price จะมีค่าเท่ากับ 3000
                price = 3000; //price จะมีค่าเท่ากับ 3000
                
            }
        }
        this.income = this.income + price; //ตัวแปร income มีค่าเท่ากับ ค่าที่อยู่ในตัวแปร income บวกกับ ค่าที่อยู่ในตัวแปร price
    }
        
        
        
        
        
        
        
    

    
}  
