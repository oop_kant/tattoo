/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.tattoo;

/**
 *
 * @author MSI
 */
public class TestTattoo {
    public static void main(String[] args) {
        Tattoo artist1 = new Tattoo("Chang Kant",0); //กำหนด object ขึ้นมาใหม่ ที่มีชื่อว่า artist1 และ ใส่ค่าในตัวแปร artisrtname และ income
        Tattoo artist2 = new Tattoo("Chang Ton",0); //กำหนด object ขึ้นมาใหม่ ที่มีชื่อว่า artist2 และ ใส่ค่าในตัวแปร artisrtname และ income
        
        artist1.details(45, 2); //กำหนดค่าของตัวแปร size และ color ที่อยู่ใน object artist1 ให้มีค่าเป็น 45 และ 2 
        artist1.details(100, 1); //กำหนดค่าของตัวแปร size และ color ที่อยู่ใน object artist1 ให้มีค่าเป็น 100 และ 1 
        artist2.details(20, 5); //กำหนดค่าของตัวแปร size และ color ที่อยู่ใน object artist1 ให้มีค่าเป็น 20 และ 5
        artist1.details(150, 2); //กำหนดค่าของตัวแปร size และ color ที่อยู่ใน object artist1 ให้มีค่าเป็น 150 และ 2
        artist2.details(200, 10); //กำหนดค่าของตัวแปร size และ color ที่อยู่ใน object artist1 ให้มีค่าเป็น 200 และ 10       
        
        System.out.println("This week,"+artist1.artistname + " has income from tattooing " + artist1.income + " Bath."); //แสดงค่ารายได้ของช่างสักกานต์ในสับดาห์นี้
        System.out.println("This week,"+artist2.artistname + " has income from tattooing " + artist2.income + " Bath."); //แสดงค่ารายได้ของช่างสักต้นในสับดาห์นี้
    }
}
